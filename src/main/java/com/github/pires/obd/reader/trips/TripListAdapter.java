package com.github.pires.obd.reader.trips;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.github.pires.obd.reader.ErrorDetails;
import com.github.pires.obd.reader.R;
import com.github.pires.obd.reader.activity.LocationAddress;
import com.github.pires.obd.reader.activity.MapDetails;

import com.github.pires.obd.reader.activity.TripListActivity;
import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.github.pires.obd.enums.AvailableCommandNames.MAF;

public class TripListAdapter extends ArrayAdapter<TripRecord> {
    /// the Android Activity owning the ListView
    TextView rowStartAdd;
    private final Activity activity;
    String dis;
    /// a list of trip records for display
    private final List<TripRecord> records ;
    private ArrayList<ArrayList<LatLng>> gps=new ArrayList<>();
    /**
     * DESCRIPTION:
     * Constructs an instance of TripListAdapter.
     *
     * @param activity - the Android Activity instance that owns the ListView.
     * @param records  - the List of TripRecord instances for display in the ListView.
     */
    public TripListAdapter(Activity activity, List<TripRecord> records) {
        super(activity, R.layout.row_trip_list, records);
        this.activity = activity;
        this.records = records;
        this.gps = gps;

    }

     /**
     *
     * DESCRIPTION:
     * Constructs and populates a View for display of the TripRecord at the index
     * of the List specified by the position parameter.
     *
     * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
     *
     /**/

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        // create a view for the row if it doesn't already exist
        if (view == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            view = inflater.inflate(R.layout.row_trip_list, null);
        }



        // get widgets from the view
        TextView startDate = (TextView) view.findViewById(R.id.startDate);
        TextView columnDuration = (TextView) view.findViewById(R.id.columnDuration);
        TextView rowEngine = (TextView) view.findViewById(R.id.rowEngine);
        TextView rowOther = (TextView) view.findViewById(R.id.rowOther);
        rowStartAdd = (TextView) view.findViewById(R.id.rowStartAdd);
        TextView rowEndAdd = (TextView) view.findViewById(R.id.rowEndAdd);


        // populate row widgets from record data
        final TripRecord record = records.get(position);

        // date
        startDate.setText(record.getStartDateString());
        columnDuration.setText(calcDiffTime(record.getStartDate(), record.getEndDate()));
        Log.d("coloumn", "timeo :" + calcDiffTime(record.getStartDate(), record.getEndDate()));
        String rpmMax = String.valueOf(record.getEngineRpmMax());

        String engineRuntime = record.getEngineRuntime();
        if (engineRuntime == null)
            engineRuntime = "None";
        rowEngine.setText("Engine Runtime: " + engineRuntime + "\tMax RPM: " + rpmMax);

        rowOther.setText("Max speed: " + String.valueOf(record.getSpeedMax()));
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(activity, MapDetails.class);

                Log.d("triplistad",record.getGps());
                intent.putExtra("gps",record.getGps());
                activity.startActivity(intent);
            }
        });

String pointsString= record.getGps();

        String[] points =  pointsString.substring(1,pointsString.length()-1).split(", ");
        LocationAddress locationAddress = new LocationAddress();
        Double latStart = Double.valueOf(points[0]);
        Double lonStart = Double.valueOf(points[1]);


        Double latEnd = Double.valueOf(points[points.length-2]);
        Double lonEnd = Double.valueOf(points[points.length-1]);

        locationAddress.getAddressFromLocation(latStart, lonStart,
                activity, new GeocoderHandler());
        rowEndAdd.setText("end Add: " + latEnd + " , " + lonEnd);
        return view;
    }

    private String calcDiffTime(Date start, Date end) {
        long diff = end.getTime() - start.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        StringBuffer res = new StringBuffer();

        if (diffDays > 0)
            res.append(diffDays + "d");

        if (diffHours > 0) {
            if (res.length() > 0) {
                res.append(" ");
            }
            res.append(diffHours + "h");
        }

        if (diffMinutes > 0) {
            if (res.length() > 0) {
                res.append(" ");
            }
            res.append(diffMinutes + "m");
        }

        if (diffSeconds > 0) {
            if (res.length() > 0) {
                res.append(" ");
            }

            res.append(diffSeconds + "s");
        }
        return res.toString();
    }

    /**
     * DESCRIPTION:
     * Called by parent when the underlying data set changes.
     *
     * @see android.widget.ArrayAdapter#notifyDataSetChanged()
     */
    @Override
    public void notifyDataSetChanged() {

        // configuration may have changed - get current settings
        //todo
        //getSettings();

        super.notifyDataSetChanged();
    }
    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }
            rowStartAdd.setText(locationAddress);
        }
    }
}
