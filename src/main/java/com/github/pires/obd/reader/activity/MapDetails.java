package com.github.pires.obd.reader.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.github.pires.obd.reader.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

public class MapDetails extends FragmentActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    private String gps;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_details);


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        gps = getIntent().getStringExtra("gps");
        ArrayList<LatLng> coordList = new ArrayList<LatLng>();

        Log.d("mapdetail", String.valueOf(gps));
        String[] points =  gps.substring(1,gps.length()-1).split(", ");
        for (int i=0; i<points.length-1; i+=2) {
            double currentLat = Double.parseDouble(points[i]);
            double currentLon = Double.parseDouble(points[i+1]);
            coordList.add(new LatLng(currentLat,currentLon));
            LatLng pos= new LatLng(currentLat,currentLon);
            googleMap.addMarker(new MarkerOptions().position(pos)
                    );
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(pos));

//            Polyline line = mMap.addPolyline(new PolylineOptions()
//                    .add(new LatLng(currentLat,currentLon) )
//            .width(5)
//                    .color(Color.BLUE));
        }
        PolylineOptions polylineOptions = new PolylineOptions();

// Create polyline options with existing LatLng ArrayList
        polylineOptions.addAll(coordList);
        polylineOptions
                .width(5)
                .color(Color.RED);

// Adding multiple points in map using polyline and arraylist
        googleMap.addPolyline(polylineOptions);
    }
}
