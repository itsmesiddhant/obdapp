/*
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.github.pires.obd.reader.net;

import android.widget.TextView;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;

/**
 * DTO for OBD readings.
 */
public class ObdReading {
    private double latitude, longitude, altitude,VSS,Enginerpm,AVG,distance,avgSpeed,avgMil,maxSpeed,maxRPM,MAF;
    private long timestamp;
    boolean mil;
     int dtc,timeInSec;
    float coolant,oil,airtemp,ambient;
    String throttle,compasso,runtime,vehicle,battery,equiratio,timing,engineload,absload,fueltype,consumption;
    private String vin,Tcs; // vehicle id
    private Map<String, String> readings;
    String fuellevel,afratio,wafratio,barometric,fuelpressure,rail,intake,distancemil,distancecc,permanettc, pendingtc;



    public ObdReading(double latitude, double longitude, double altitude, String vin, long timestamp,
                      double VSS, double Enginerpm,double avgSpeed,double avgMil,double maxSpeed,double maxRPM,int timeInSec, double AVG, double distance, double MAF, String Tcs, boolean mil, int dtc, float coolant, String throttle, String compasso, String runtime, String vehicle, String battery, String equiratio, String timing, String engineload, String absload,
                      float oil, String fueltype, String consumption, String fuellevel, String wafratio, String barometric, String fuelpressure,
                      String rail, String intake,float airtemp,float ambient,String distancemil,String distancecc ,String permanettc, String pendingtc, Map<String, String> temp) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.timeInSec = timeInSec;
        this.vin = vin;
        this.timestamp = timestamp;
        this.VSS = VSS;
        this.Enginerpm = Enginerpm;
        this.avgSpeed = avgSpeed;
        this.avgMil = avgMil;
        this.maxRPM = maxRPM;
        this.maxSpeed = maxSpeed;
        this.AVG = AVG;
        this.distance = distance;
        this.MAF = MAF;
        this.Tcs = Tcs;
        this.mil = mil;
        this.dtc= dtc;
        this.coolant= coolant;
        this.throttle= throttle;
       this.compasso=compasso;
        this.runtime=runtime;
        this.vehicle=vehicle;
        this.battery=battery;
        this.equiratio=equiratio;
        this.timing=timing;
        this.engineload=engineload;
        this.absload=absload;
        this.oil=oil;
        this.fueltype=fueltype;
        this.consumption=consumption;
        this.fuellevel=fuellevel;
        //this.afratio=afratio;
        this.wafratio=wafratio;
        this.barometric=barometric;
        this.fuelpressure=fuelpressure;
        this.rail=rail;
        this.intake=intake;
        this.airtemp=airtemp;
        this.ambient=ambient;
        this.distancemil=distancemil;
        this.distancecc=distancecc;
        this.permanettc=permanettc;
        this.pendingtc=pendingtc;

    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }



    public String getVin() {
        return vin;
    }

    public void setVin(String vehicleid) {
        this.vin = vehicleid;
    }


    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public double getVSS() {
        return VSS;
    }

    public void setVSS(double vss) {
        this.VSS = vss;
    }
    public double getEnginerpm() {
        return Enginerpm;
    }

    public void setEnginerpm(double enginerpm) {
        this.Enginerpm = enginerpm;
    }
    public double getAVG() {
        return AVG;
    }

    public void setAVG(double AVG) {
        this.AVG = AVG;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }



    public double getMAF() {
        return MAF;
    }

    public void setMAF(double MAF) {
        this.MAF = MAF;
    }



    public String getTcs() {
        return Tcs;
    }




    public void setTcs(String tcs) {
        this.Tcs = tcs;
    }
}


