package com.github.pires.obd.reader;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.github.pires.obd.reader.activity.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

public class ErrorDetails extends Activity {
public TextView texto,texto1,texto2;
    String code;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error_details);

        texto = (TextView)findViewById(R.id.error);
        texto1 = (TextView)findViewById(R.id.error2);
        texto2 = (TextView)findViewById(R.id.error3);
        Intent i = getIntent();
        code=i.getStringExtra("code");
        PostAsync postAsync= new PostAsync();

        postAsync.execute(code);
        texto.setText(code);



    }













    //------------------------------jsonparser class---------------------------------------------------------

    public class JSONParser {

        String charset = "UTF-8";
        HttpURLConnection conn;
        DataOutputStream wr;
        StringBuilder result;
        URL urlObj;
        JSONObject jObj = null;
        StringBuilder sbParams;
        String paramsString;

        public JSONObject makeHttpRequest(String url, String method,
                                          HashMap<String, String> params) {

            sbParams = new StringBuilder();
            int i = 0;
            for (String key : params.keySet()) {
                try {
                    if (i != 0){
                        sbParams.append("&");
                    }
                    sbParams.append(key).append("=")
                            .append(URLEncoder.encode(params.get(key), charset));

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                i++;
            }

            if (method.equals("POST")) {
                // request method is POST
                try {
                    urlObj = new URL(url);

                    conn = (HttpURLConnection) urlObj.openConnection();

                    conn.setDoOutput(true);

                    conn.setRequestMethod("POST");

                    conn.setRequestProperty("Accept-Charset", charset);

                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);

                    conn.connect();

                    paramsString = sbParams.toString();

                    wr = new DataOutputStream(conn.getOutputStream());
                    wr.writeBytes(paramsString);
                    wr.flush();
                    wr.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else if(method.equals("GET")){
                // request method is GET

                if (sbParams.length() != 0) {
                    url += "?" + sbParams.toString();
                }

                try {
                    urlObj = new URL(url);

                    conn = (HttpURLConnection) urlObj.openConnection();

                    conn.setDoOutput(false);

                    conn.setRequestMethod("GET");

                    conn.setRequestProperty("Accept-Charset", charset);

                    conn.setConnectTimeout(15000);

                    conn.connect();

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            try {
                //Receive the response from the server
                InputStream in = new BufferedInputStream(conn.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }

                Log.d("JSON Parser", "result: " + result.toString());

            } catch (IOException e) {
                e.printStackTrace();
            }

            conn.disconnect();

            // try parse the string to a JSON object
            try {
                jObj = new JSONObject(result.toString());
            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }

            // return JSON Object
            return jObj;
        }
    }

    //--------------------------------------------------------------------------------------------------------


    //---------------using post method-------------------------------------------------------------------------

    class PostAsync extends AsyncTask<String, String, JSONObject> {

        JSONParser jsonParser = new JSONParser();

        private ProgressDialog pDialog;

        private static final String LOGIN_URL = "http://www.carpiko.com/php/getCodeDetail.php";

        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";


        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ErrorDetails.this);
            pDialog.setMessage("Signing in ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... args) {

            try {

                HashMap<String, String> params = new HashMap<>();
                params.put("code", args[0]);


                Log.d("request", "starting");

                JSONObject json = jsonParser.makeHttpRequest(
                        LOGIN_URL, "GET", params);

                if (json != null) {
                    Log.d("JSON result", json.toString());

                    return json;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(JSONObject json) {

            String success = "0";
            String message = "";
            String sol = "";



            if (json != null) {
                //  Toast.makeText(SignupActivity.this, json.toString(), Toast.LENGTH_LONG).show();

                try {
                    success = json.getString(TAG_SUCCESS);
                    message = json.getString(TAG_MESSAGE);
                    sol = json.getString("solution");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (success.equals("1"))
            {
                texto1.setText(message);
                texto2.setText(sol);

                Log.d("Success!", message);
            }else{


                if(message.equals(""))
                    Toast.makeText(getApplicationContext()," Network error. Try again !", Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(getApplicationContext(),message, Toast.LENGTH_LONG).show();

                Log.d("Failure", message);
            }
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.dismiss();
            }
        }

    }
    //---------------------------------------------------------------------------------------------------------
}
